## Gus

* **CTF:** Nahamcon CTF 2021
* **Category:** OSINT
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
This is Stage 1 of Path 4 in The Mission. After solving this challenge, you may need to refresh the page to see the newly unlocked challenges.

Use open-source intelligence to track down information on Gus.

With the flag of this challenge, you should also find details you can use in later challenges.
```
---

### Solution
I already had Gus' full name (Gus Rodry) from the "Meet the Team" challenge. I used [spiderfoot](https://www.spiderfoot.net/) to search by his name. This returned his github at https://github.com/gusrodry/

Some futher poking around found one of his repos with a flag in plain sight [here](https://github.com/gusrodry/development/commit/7107e8987b7a87874880c03825dbf925af510434).

---

### Flag 
```
flag{84d5cc7e162895fa0a5834f1efdd0b32}
```
