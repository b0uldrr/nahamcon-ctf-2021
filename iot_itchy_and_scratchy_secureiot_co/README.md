## IoT Itchy & Scratchy SecureIoT Co

* **CTF:** Nahamcon CTF 2021
* **Category:** IoT
* **Points:** 480
* **Author(s):** b0uldrr
* **Date:** 14/04/21

---

### Challenge
```
Brought to you by IoT Village.

See if you have the patience to scratch the itch; patience is key. Mosquitto bites are so annoying! Except, in this case.. you can become an admin.

Connect here: 35.225.10.98:1883 with iot:iot
and here: http://35.225.10.98
```

---

### Solution
This one was similar to the earlier "IoT Itchy" challenge except we were also given a website at http://35.225.10.98. Visiting this site displayed a username/password login form with a One-Time-Password field at the bottom.

I signed into the MQTT server with the provided credentials and lots of topics started to roll in. Under the `SecureCo/device/admin/login` topic I found a base64 encoded username and password combo which decoded to `administrator:SeCUReP@55W0rD1`, but we still needed the OTP to be able to login.

![mqtt](images/mqtt.png)

Under the `SecureCo/webcam/feed` topic there were 2 long base64 encoded strings split into `part1` and `part2`. I copied them into my terminal as a single string, piped them to `base64` to decode and then redirected the output to a file.

`echo <part1><part2> | base64 -d > output`

Running `file` over the output showed it was a jpg image. Opening the image displayed a picture of a phone with the OTP code we needed to login. Logging in displayed the flag.

![OTP](images/OTP.jpg)

![flag](images/flag.png)

---

### Flag 
```
flag{La2y_aDm1N5_5H0uLD_N07_U53_IoT}
```
