## pollex

* **CTF:** Nahamcon CTF 2021
* **Category:** Misc
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
Download the file below.

Some people seem to have trouble reading this, understandably so. Sorry. The flag ends in these characters: 8fe36bc00}
```

### Downloads
* [pollex](pollex)

---

### Solution

Running `file` on the downloaded file shows it is a jpeg image.

![pollex](images/pollex.jpg)

Running `binwalk` on the image shows there are more images concealed within this image.

```
$ binwalk pollex 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
30            0x1E            TIFF image data, little-endian offset of first image directory: 8
334           0x14E           JPEG image data, JFIF standard 1.01
364           0x16C           TIFF image data, little-endian offset of first image directory: 8
848           0x350           JPEG image data, JFIF standard 1.01
6731          0x1A4B          Copyright string: "CopyrightOwner> <rdf:Seq/> </plus:CopyrightOwner> <plus:Licensor> <rdf:Seq/> </plus:Licensor> <dc:creator> <rdf:Seq> <rdf:li>Ste"
6765          0x1A6D          Copyright string: "CopyrightOwner> <plus:Licensor> <rdf:Seq/> </plus:Licensor> <dc:creator> <rdf:Seq> <rdf:li>Stevanovic Igor</rdf:li> </rdf:Seq> <"
```

I extracted those images using `binwalk --dd='.*' pollex`. Navigating into the extracted file folder, I found the hidden images which contained the flag.

![flag](images/flag.jpg)

---

### Flag 
```
flag{65c34a1ec121a286600ddd48fe36bc00}
```
