## Nahamcon CTF 2021

* **CTF Time page:** https://ctftime.org/event/1281 
* **Category:** Jeopardy
* **Date:** Fri, 12 March 2021, 20:00 UTC — Sun, 14 March 2021, 20:00 UTC

---

### Solved Challenges
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|car keys|crypto|50|keyed ceasar cipher|
|gus|OSINT|50|spiderfoot, OSINT|
|iot itchy|iot|456|mqtt|
|iot itchy & scratchy|iot|480|mqtt, base64|
|ret2basic|pwn|59|BOF, 64bit|
|esab64|crypto|50|base64, rev|
|meet the team|web|50|git|
|pollex|stego|50|stego, binwalk, jpg|

### Unsolved Challenges to Revisit
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|the list|pwn|452|BOF|
|parsel tongue|rev|379|compiled python, rev|
