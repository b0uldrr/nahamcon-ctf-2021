## Car Keys

* **CTF:** Nahamcon CTF 2021
* **Category:** Crypto
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
We found this note on someone's key chain! It reads... ygqa{6y980e0101e8qq361977eqe06508q3rt}? There was another key that was engraved with the word QWERTY, too... 
```

---

### Solution
I searched for different types of keyed transposition ciphers until I found the `keyed ceasar cipher`. I used [this website](https://www.boxentriq.com/code-breaking/keyed-caesar-cipher) to solve it.

---

### Flag 
```
flag{6f980c0101c8aa361977cac06508a3de}
```
