Unsovled. Revisit for practice.

## the list

* **CTF:** Nahamcon CTF 2021 
* **Category:** Binary exploitation
* **Points:** 452 
* **Author(s):** Unsolved...
* **Date:** N/A

---

### Challenge
```
We need you to compile a list of users for the event. Here's a program you can use to help.

Download the file below and ress the Start button on the top-right to begin this challenge.
```

### Downloads
* [the_list](the_list)
