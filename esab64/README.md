## esab64

* **CTF:** Nahamcon CTF 2021 
* **Category:** Crypto
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
Was it a car or a cat I saw?

Download the file below.
```

### Downloads
* [esab64](esab64)

---

### Solution
The downloaded file contains a single line of text, `mxWYntnZiVjMxEjY0kDOhZWZ4cjYxIGZwQmY2ATMxEzNlFjNl13X`. The solution was to reverse the text, decode it from base64, and then reverse the output.

We can do this in one line with `rev esab64 | base64 -d | rev`.

---

### Flag 
```
flag{fb5211b498afe87b1bd0db601117e16e}
```
