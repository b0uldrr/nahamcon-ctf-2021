## Meet the Team

* **CTF:** Nahamcon CTF 2021
* **Category:** OSINT
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
Recover the list of employees working at CONSTELLATIONS.

With the flag of this challenge, you should find new information that will help with future challenges.

You should find the flag for this challenge ON THIS constellations.page website. You will not find it on GitHub.

HINT: "Can we please stop sharing our version control software out on our website?"

HINT AGAIN: you are looking for a publicly accessible version control software folder published on the constellations.page website itself

After solving this challenge, you may need to refresh the page to see the newly unlocked challenges.
```
---

### Solution
The challenge website (https://constellations.page/meet-the-team.html) was provided from solving an ealier challenge. Visiting the page, we find a message that the "Meet the Team" page has been redacted. At the top of the page source, we find a comment `<!-- Vela, can we please stop sharing our version control software out on the public internet? -->`, while at the bottom of the page in the socials, we find a link to the company Github page (along with Twitter, Facebook, etc.)

![redacted](images/redacted.png)
![comment](images/comment.png)

So we know that the company is using git & github for their version control. After a bit of research into [git folder exposure vulnerabilities](https://medium.com/@roshancp/source-code-disclosure-via-exposed-git-folder-d22919c590a2), I tried to see if the git repo was accidentally left in the production folders by visiting https://constellations.page/.git. This url returned a `Forbidden` result, so we know the resource exists but we don't have permission to view it.

I downloaded [GitHacker](https://github.com/captain-noob/GitHacker) and used it to download the forbidden Constellations repo with `python3 GitHacker.py https://constellations.page/.git`

Navigating into the downloaded Constellations repo, I used `git log` to see the history of commits.

![git_log](images/git_log.png)

We can see that there have been 3 commits to this repo. The first was just the initial assets and landing page. The second commit (with hash 4c88ac1c56fe228267cf415c3ef87d7c3b8abd60) contained the first version of the `meet-the-team.html` page, and the third commit redacted the info from that page.

To view the original copy of the `meet-the-team.html` page, I used `git show 4c88ac1c56fe228267cf415c3ef87d7c3b8abd60:meet-the-team.html`. Scrolling down through the page, we find the names and roles of the Constellation staff along with the flag.

![flag](images/flag.png)

---

### Flag 
```
flag{4063962f3a52f923ddb4411c139dd24c}
```
