## IoT Itchy

* **CTF:** Nahamcon CTF 2021
* **Category:** IoT
* **Points:** 456
* **Author(s):** b0uldrr
* **Date:** 14/03/21

---

### Challenge
```
Brought to you by IoT Village.

See if you have the patience to scratch the itch; patience is key. Mosquitto bites are so annoying!

Connect here: 34.70.255.20:1883 with iot:iot
```

---

### Solution
"Mosquitto" is a reference to the MQTT protocol, and port 1883 is the standard MQTT port. I scanned the provided IP:port and sure enough there was an MQTT server running.

![nmap](images/nmap.png)

I downloaded and used [MQTT Explorer](http://mqtt-explorer.com/) to connect to the server with the provided credentials. Every few seconds a new MQTT message would be published. The topic at Kids-room-1/Toy4/Ing contained the flag.

![flag](images/flag.png)


---

### Flag 
```
flag{fh1nd1n9_R4nd0m_5uBt0P1c5_i5_fuN}
```
